﻿
public static class ScriptableObjectAssetMenu
{
    public const int        ORDER_SHARED_VARIABLE_INT           = 0;
    public const string     MENU_SHARED_VARIABLE_INT            = "FAITH/Shared Variable/IntVariable";

    public const int        ORDER_SHARED_VARIABLE_FLOAT         = 1;
    public const string     MENU_SHARED_VARIABLE_FLOAT          = "FAITH/Shared Variable/FloatVariable";

    public const int ORDER_SHARED_VARIABLE_RANGE                = 2;
    public const string MENU_SHARED_VARIABLE_RANGE              = "FAITH/Shared Variable/RangeVariable";

    public const int ORDER_SCENE_MANAGEMENT_SCENE_VARIABLE      = 3;
    public const string MENU_SCENE_MANAGEMENT_SCENE_VARIABLE    = "FAITH/Shared Variable/SceneVariable";
    //----------------------


    public const int        ORDER_ACCOUNT_MANAGER_SETTINGS      = 0;
    public const string     MENU_ACCOUNT_MANAGER_SETTINGS       = "FAITH/Settings/AccountManagerSetting";

    //----------------------

    public const int ORDER_SCENE_CONTAINER = 1;
    public const string MENU_SCENE_CONTAINER = "FAITH/Scene/SceneContainer";

    //----------------------

    public const int        ORDER_VISUAL_TRANSFORM_ANIMATOR     = 0;
    public const string     MENU_VISUAL_TRANSFORM_ANIMATOR      = "FAITH/Visual/TransformAnimator";

}



