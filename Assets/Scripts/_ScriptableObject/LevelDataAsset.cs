﻿using UnityEngine;
using UnityEngine.Events;
using com.faith.core;

[CreateAssetMenu(fileName = "LevelDataAsset", menuName = "RailShooter/Asset Container/LevelDataAsset")]
public class LevelDataAsset : ScriptableObject
{

    #region Public Variables


    [Range(1, 1000)]
    public int maxLevel = 1;

    public SharedData sharedGameData;



    #endregion

    #region Private Varaibles

    private bool _isDataLoaded;
    private SavedData<int> _levelIndex;
    private UnityAction OnLevelDataLoaded;

    #endregion

    #region Configuretion

    private void OnLevelIndexLoaded(int _loadedDayIndex)
    {

        sharedGameData.levelIndex = _loadedDayIndex;
        if (!_isDataLoaded)
        {
            _isDataLoaded = true;
            OnLevelDataLoaded.Invoke();
        }
    }

    private bool IsValidLevelIndex(int dayIndex)
    {

        if (dayIndex >= 0 && dayIndex < maxLevel)
        {

            return true;
        }

        return false;
    }

    #endregion

    #region Public Callback

    public void Initialization(UnityAction OnLevelDataLoaded)
    {

        _isDataLoaded = false;
        this.OnLevelDataLoaded = OnLevelDataLoaded;

        sharedGameData = new SharedData();
        _levelIndex = new SavedData<int>("LEVEL_INDEX", 0, OnLevelIndexLoaded);

    }

    public void GotToNextLevel()
    {

        int nextDay = _levelIndex.GetData() + 1;
        if (IsValidLevelIndex(nextDay))
            _levelIndex.SetData(nextDay);
        else
        {
            _levelIndex.SetData(0);
        }
    }

    #endregion
}
