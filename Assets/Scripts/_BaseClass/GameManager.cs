﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faith.core;

[RequireComponent(typeof(GameConfiguratorManager))]
[DefaultExecutionOrder(Constant.EXECUTION_ORDER_GAMEMANAGER)]
public class GameManager : RailShooterBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        Initialization();
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS




    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variable

    public static GameManager Instance;

#if UNITY_EDITOR

    public bool showDeveloperPanel;
    public GameState gameState;

#endif

    public new bool IsInputSystemInterrupted
    {
        get { return BaseInputSystem.IsInputSystemInterrupted; }
    }

    public GameState CurrentGameState
    {
        get { return gameStateController.gameState; }
    }

    public GameState PreviousGameState
    {
        get { return gameStateController.GetPreviousStateFromStack(); }
    }

    public SharedData SharedGameData
    {
        get { return levelData.sharedGameData; }
    }

    public LevelDataAsset levelData;

    [Header("Parameter  :   GameConfig")]
    public GameConfiguratorAsset configAssetForFeatureUnderDevelopment;
    public GameConfiguratorAsset configAssetForInputSystem;

    [Header("Parameter  :   GameState")]
    public GameStateControllerAsset gameStateController;

    [Header("Parameter  :   SceneLoad")]
    public float initialDelayForSceneLoaded;

    [Header("Parameter  :   UIAnimation")]
    public TransformAnimatorAsset centralAnimatorForUIAppear;
    public TransformAnimatorAsset centralAnimatorForUIDisappear;
    public TransformAnimatorAsset centralAnimatorForUIInteract;

    [Header("Parameter  :   Animation")]
    public TransformAnimatorAsset centralAnimatorForAppear;
    public TransformAnimatorAsset centralAnimatorForDisappear;

    #endregion

    #region Private Variables

    private static bool _isInitialDataLoaded = false;

    [SerializeField] private List<RailShooterBehaviour> _listOfRealShooterBehaviour;

    #endregion

    #region Configuretion

    private void Initialization()
    {
        Instance = this;

        foreach (RailShooterBehaviour railShooterBehaviour in _listOfRealShooterBehaviour)
        {
            railShooterBehaviour.Register();
        }

        if (!_isInitialDataLoaded)
        {

            levelData.Initialization(delegate {

                gameStateController.ChangeGameState(GameState.DataLoaded);
                gameStateController.ChangeGameState(GameState.SceneLoaded);
                _isInitialDataLoaded = true;
            });
        }
    }

    #endregion

    #region PublicCallback

    public void ChangeGameState(GameState gameState)
    {
        gameStateController.ChangeGameState(gameState);
    }

    public void LoadScene(
        SceneReference sceneReference,
        UnityAction OnSceneLoaded = null,
        float initialDelayToInvokeOnSceneLoaded = -1)
    {

        initialDelayToInvokeOnSceneLoaded = initialDelayToInvokeOnSceneLoaded == -1 ? initialDelayForSceneLoaded : initialDelayToInvokeOnSceneLoaded;

        sceneReference.LoadScene(
            OnUpdatingProgression: null,
            OnSceneLoaded: () => {
                gameManager.ChangeGameState(GameState.SceneLoaded);
                OnSceneLoaded?.Invoke();
            },
            initalDelayToInvokeOnSceneLoaded: initialDelayToInvokeOnSceneLoaded);


    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS
}
